use anyhow::{Context, Result};
use cargo_toml::Manifest;
use std::fs;
use std::path::Path;

pub struct CargoManifest(Manifest);

impl CargoManifest {
    pub fn read(path: Option<&Path>) -> Result<Manifest> {
        match path {
            Some(path) => read_manifest_from_path(path),
            None => read_from_current_dir(),
        }
    }
}

fn read_from_current_dir() -> Result<Manifest> {
    read_manifest("Cargo.toml")
}

fn read_manifest<P: AsRef<Path> + Sized>(arg: P) -> Result<Manifest> {
    let cargo_toml_bytes = fs::read(arg)?;
    Manifest::from_slice(&cargo_toml_bytes).context("failed to read cargo manifest")
}

fn read_manifest_from_path<P: AsRef<Path> + Sized>(path: P) -> Result<Manifest> {
    read_manifest(path)
}
