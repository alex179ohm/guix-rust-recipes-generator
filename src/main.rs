use anyhow::Result;
mod manifest;
use manifest::CargoManifest;

fn main() -> Result<()> {
    let manifest = CargoManifest::read(None)?;
    println!("{:?}", manifest);
    Ok(())
}
